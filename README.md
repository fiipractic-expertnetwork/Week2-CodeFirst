# CodeFirst solution

CodeFirst solution having class library project (CodeFirst.Data) for FII Practic (Expert Network .NET Core Training)

### Prerequisites

.NET Core 2.0

Entity Framework Core 2.0

Visual Studio 2017

## Authors

* **Ciubotariu Florin-Constantin** - (ciubotariu.florin@gmail.com)
* **Moniry-Abyaneh Daniel** - (daniel.moniry@gmail.com)