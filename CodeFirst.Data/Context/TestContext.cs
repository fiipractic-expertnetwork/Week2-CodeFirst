﻿using CodeFirst.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace CodeFirst.Data.Context
{
  public class TestContext : DbContext
  {
    public DbSet<User> Users { get; set; }
    public DbSet<Article> Articles { get; set; }
    public DbSet<Tag> Tags { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<ArticleTag>().HasKey(t => new { t.ArticleId, t.TagId });
      modelBuilder.Entity<ArticleTag>().ToTable("ArticleTags");
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.UseSqlServer("Data Source=office.expertnetwork.ro,14334;Initial Catalog=DanielTest;User Id=fiipractic1;Password=jYnLUUBALrbI4vsSVh7l;");
    }
  }
}
